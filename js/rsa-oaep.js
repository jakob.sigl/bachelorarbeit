async function RSA_OAEP_encrypt(key, plaintext) {
    let enc = new TextEncoder();
    encoded = enc.encode(plaintext);
    var ciphertext = await window.crypto.subtle.encrypt(
        {
            name: "RSA-OAEP"
        },
        key,
        encoded
    )
    return arrayBufferToHexString(ciphertext);
}

async function RSA_OAEP_decrypt(key, ciphertext) {
    var plaintext = await window.crypto.subtle.decrypt(
        {
            name: "RSA-OAEP"
        },
        key,
        hexStringToArrayBuffer(ciphertext)
    )
    return arrayBufferToAscii(plaintext);
}