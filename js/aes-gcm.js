async function AES_GCM_encrypt(key, plaintext) {
    console.log("Verschlüsselung mit:\n" + "Key: " + key + ",\n" + "Klartext: " + plaintext);
    var ciphertext = await window.crypto.subtle.encrypt(
        {
            name: "aes-gcm",
            iv: iv
        }, 
        key, 
        asciiToArrayBuffer(plaintext)
    );
    return arrayBufferToHexString(ciphertext);
}

async function AES_GCM_decrypt(key, ciphertext) {
    console.log("Entschlüsselung mit:\n" + "Key: " + key + ",\n" + "Chiffrat: " + ciphertext);
    var plaintext = await window.crypto.subtle.decrypt(
        {
            name: "aes-gcm", 
            iv: iv
        }, 
        key, 
        hexStringToArrayBuffer(ciphertext)
    );
    return arrayBufferToAscii(plaintext);
}  