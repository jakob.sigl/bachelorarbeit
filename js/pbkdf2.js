function getKeyMaterial(password) {
    let enc = new TextEncoder('utf-8');
    return window.crypto.subtle.importKey(
    "raw", 
    enc.encode(password), 
    {name: "PBKDF2"}, 
    false, 
    ["deriveBits", "deriveKey"]
    );
}

function getKey(keyMaterial, salt, iterations) {
    return window.crypto.subtle.deriveKey(
    {
        "name": "PBKDF2",
        salt: salt, 
        "iterations": iterations,
        "hash": "SHA-256"
    },
    keyMaterial,
    { "name": "AES-GCM", "length": 256},
    true,
    [ "encrypt", "decrypt" ]
    );
}

async function deriveMasterKey(password) {
    var salt = saltBuffer1;
    var iterations = "100000";
    var keyMaterial = await getKeyMaterial(password);
    var key = await getKey(keyMaterial, salt, iterations);
    console.log("MK aus:\n" + "Key: " + password + "\n" + ", Salt: " + salt + "\n" + ", Iterationen: "  + iterations);
    return key;
}

async function deriveAuthHash(password) {
    var salt = saltBuffer1;
    var iterations = "1";
    var keyMaterial = await getKeyMaterial(password);
    var key = await getKey(keyMaterial, salt, iterations);
    console.log("LoginAH aus:\n" + "Key: " + password + "\n" + ", Salt: " + salt + "\n" + ", Iterationen: "  + iterations);
    return key;
}

async function deriveAuthKey(password) {
    var salt = saltBuffer2;
    var iterations = "100000";
    var keyMaterial = await getKeyMaterial(password);
    var key = await getKey(keyMaterial, salt, iterations);
    console.log("LoginAK aus:\n" + "Key: " + password + "\n" + ", Salt: " + salt + "\n" + ", Iterationen: "  + iterations);
    return key;
}